mkdir -p ~/deps
cd ~/deps
if [[ ! -e peak-linux-driver-8.6.0 ]]; then
  wget http://www.peak-system.com/fileadmin/media/linux/files/peak-linux-driver-8.6.0.tar.gz
  tar -xf peak-linux-driver-8.6.0.tar.gz
  rm peak-linux-driver-8.6.0.tar.gz
  cd peak-linux-driver-8.6.0
else
  cd peak-linux-driver-8.6.0
  make clean
fi

make NET=NETDEV_SUPPORT pcc=NO
if [ -e /usr/lib/libpcan.so ]; then
  sudo make uninstall
fi 
sudo make install
